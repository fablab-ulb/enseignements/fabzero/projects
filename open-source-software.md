# FOSS - Free and Open Source Software FOSS

[What is FOSS ?](https://itsfoss.com/what-is-foss/) ([Wikipedia](https://en.wikipedia.org/wiki/Free_and_open-source_software))
  * source code can be used, studied, copied, modified and redistributed (with/without restriction).

## Open-source vs proprietary softwares

> learn to recognize an open-source software and a proprietary software

| Open software         | Proprietary     |
|--------------|-----------|
| Linux, Ubuntu, OpenOffice.org | Windows, Mac Os, Microsoft office      |
| Purchased **with** its source code      | Purchased **without** its source code |
| decentralized development model   | centralized development model  |
|  users have the rights to use, study, change, and distribute the software and its source code to anyone and for any purpose.  |  Users have only the rights to use the software and cannot study, modify and share the software ! |
|  mostly free, sometimes paid  | mostly paid, sometimes free  |
|  can install freely on any computer | must have a licence from vendor  |
|  No one is responsible to the software  | the vendor is responsible  |
|  support from the community  | support from the vendor  |
|  guarantee that it will work and get support in the long term  | guarantee that it will work and get support in the short term  |
|  everyone has the right to modify and tweak the code for their needs  | you can experiment with a demo before purhase  |

## Licences

> Learn about code licenses and how to use them.
> build on other people's code and give proper credits.

* [Creative Commons License](https://creativecommons.org/about/cclicenses/)    
* [Free software](https://www.gnu.org/philosophy/free-sw.html), [licences](https://www.gnu.org/licenses/license-list#SoftwareLicenses)
* [OSI Approved Licenses](https://opensource.org/licenses/)
* [Writing code guidelines](https://integrity.mit.edu/handbook/writing-code)
* Licensed code example ([code header](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields/-/blob/master/PFC-Headband-Light-3DPrint/anti_projection.scad), [LICENSE.md](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields/-/blob/master/PFC-Headband-Light-3DPrint/LICENSE.md)) from the [protecive face shield project @ FabLab ULB during the 2020 covid crisis](https://fablab-ulb.gitlab.io/projects/coronavirus/protective-face-shields/).