# 3D Modeling Screw Tolerance Box


## Tolerance

When working on a 3D printed project that requires screws (printed or non-printed), tolerance is something you need to consider. Depending on the printer, this tolerance may vary.

In the case of the FabLab ULB, where we only have the Prusa i3 MK3S Plus, these tests were conducted to determine the tolerance you should use when designing your parts.

To determine the necessary tolerance, **gently** screw one of your screws into the corresponding M3, M4, M5, or M6 cylinders. Each cylinder is marked with the tolerance it was designed for (Fusion 360).

**What is Tolerance?**
Tolerance is the space between two fitting parts. It accounts for imperfections, expansion, or contraction.

![Screw tolerence](Panel/img/screwTolerence.png)



## Fusion 360: 3D Modeling Screws


* If you want to model an M6 screw, whether positive or negative, start by modeling a cylinder with a 6mm diameter.

![cilinder extrude](Panel/img/cilinderExtude.png)

* In Solid → Create → Thread feature.

* Select the wall of your 6mm diameter cylinder.

* Check the box for Modeled to see the rendered result of your operation. Leave all other options as is. This will result in a standard M6 screw.

![automatic tolerence](Panel/img/automaticTolerence.png)


As you can see, the now-threaded cylinders have a gap between them, even though they started as equal 6mm diameter cylinders. Fusion 360 applies an automatic tolerance of 0.1mm to your threads.

(If you've tested your screw in a cylinder with a 0.1mm tolerance and you are satisfied with the result, you don’t need to make any further modifications to your 3D model.)

**Adding Tolerance**

If you prefer a larger tolerance, you need to add another feature.

* Select all faces of your outward thread until the entire thread is highlighted in blue.

![positive cilinder](Panel/img/selectFaces.png)

* Go to Modify → Offset Face.

![select offset](Panel/img/selectFaceOffset.png)

**Keep in mind that Fusion 360 already applies a 0.1mm tolerance by default**. If you want a result with a 0.2mm tolerance, set the offset distance to -0.1mm:

- For 0.2mm tolerance, set the Distance of Offset Face to -0.1mm.
- For 0.3mm tolerance, set the Distance to -0.2mm.
- For 0.4mm tolerance, set the Distance to -0.3mm, and so on...


![offset distance](Panel/img/offsetDistance.png)

Result with Offset Face:

![select offset](Panel/img/withFaceOffset.png)

Result without Offset Face:

![select offset](Panel/img/noFacseOffset.png)

