# Making of the Box

![box](Panel/img/box.jpg)

## List of Materials

- Plywood: 9 mm thickness
- Plexiglass: 3 mm thickness
- MDF: 12 mm thickness

## Software Used

- Fusion 360
- Inkscape

## Getting Started

Both the Shaper machine and the Epilog laser cutter read .svg format.

To export .svg files from Fusion 360, you can install a dedicated plugin. This plugin is useful not only for Shaper machines but also for laser cutters and other devices that read .svg files.


Follow this [Instructables](https://www.instructables.com/Export-a-SVG-File-in-Fusion-360/) tutorial to install the plugin.

## Design


The purpose of the box is to store test tolerance screw cylinders.


The box is designed to organize 12 cylinders—3 for each screw "M" type (M3, M4, M5, M6). To ensure the cylinders remain organized and secure, the box is made of three layers:

![boxRendered](Panel/img/boxRendered.png)


- Top Layer: A sliding door made of plexiglass for visibility.
- Middle Layer: Plywood with 12 holes and engraved labels for intuitive organization.
- Bottom Layer: A simple plywood base of the same size as the middle layer.

## Materials and Manufacturing Techniques


The box is made of three different materials (plexiglass, plywood, and MDF), each requiring distinct fabrication methods.

![boxExploded](Panel/img/boxExploded.png)


**(1) Plexiglass Lid**

1. Export the top view of the lid from Fusion 360 as an .svg file.


![svgPlexi](Panel/img/exportToSVGPlexi.png)


Open the .svg file in Inkscape and prepare it for the laser cutter. **Ensure that the circle has a different colour than the rectangule** (circle must be cut before the rectangle).

Make the page larger in Inkscape by adding a few millimeters to the right and bottom edges to ensure all vectors are recognized by the laser cutter.

File &rarr; Document Properties...

![svgPlexi](Panel/img/inkScapePlexi.png)

Test Fit: Before cutting the final plexiglass piece, perform a test cut on scrap cardboard to verify the dimensions and ensure compatibility with the MDF frame and grooves.

![cardboardTest](Panel/img/cardboardTest.jpg)

Tiny Test: Test the frequency, speed, and power settings on a small section of plexiglass to confirm optimal cutting parameters.

Cutting Parameters: For the 3 mm thick plexiglass, the following settings were used:

-speed 10
-power 80
-frequency 100
-cycles 2



**(2) 9 mm Plywood**

1. Export the file from Fusion 360 in the same way as for the plexiglass.

2. Open the .svg file in Inkscape and add text labels to be engraved on the plywood.

3. Since the plywood is too thick for the Epilog to cut through, use the laser cutter to create clean guide lines.

4. Complete the rectangular contour with a jigsaw and drill the 12 holes manually. Use a file to refine the holes to match the laser-cut guide lines.

![SVGfilePlywood](Panel/img/plywoodSVG.png)


**(3) MDF Base**

1. In Fusion, Place all 4 mdf pieces facing Up in the position you image to cut them later with the Shaper. 

![imageExportingShaperMDF](Panel/img/mdfSidesPlaceInFusion.jpg)

2. Export the MDF parts as .svg files from Fusion 360 and import them into the Shaper software.

![imageExportingShaperMDF](Panel/img/SVGmdfSides.jpg)

3. First, operate the pocket cuts, then cut the contours.

**Assembly**

1. Glue the MDF pieces to each other and to the plywood using wood glue for reinforcement.

2. When dry, sand MDF and edges of plywood.

3. Slide the plexiglass lid into place.


	

	


 
























