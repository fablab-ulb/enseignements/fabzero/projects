# Open-source lab and frugal science projects

## Free and open-source software and hardware

* FOSS - Free and Open Source Software FOSS
  * source code can be used, studied, copied, modified and redistributed (with/without restriction).
* Free and Open-Source Hardware FOSH : FOSS principles and practices now applied to hardware
  * opportunity : reduce research cost and improve scientific tools
  * **open-source hardware** vs **locked-down proprietary tools**
  * customizable, complete control, sustainable, can repair, lower costs, critical if vendors go out of business
  * open vs free

## Open-source and frugal scientific tools:

* Scientific tools :
  * [PlanktoScope](https://www.planktoscope.org/),  a frugal high-throughput microscope platform
  * Foldscope([website](https://foldscope.com/), [PLOS ONE](https://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0098781), [Ted Talk](https://www.ted.com/talks/manu_prakash_a_50_cent_microscope_that_folds_like_origami))
  * Paperfuge([Nature paper](https://www.nature.com/articles/s41551-016-0009/), [Ted Talk](https://www.ted.com/talks/manu_prakash_lifesaving_scientific_tools_made_of_paper))
  * [3D Paws](https://www.comet.ucar.edu/node/543), 3D printed automatic weather station
  * [OpenFlexure Microscope](https://openflexure.org/)
  * [Open-Labware.net](https://open-labware.net/), 3-D print your own lab equipment
  * Open-source lab jack ([thingiverse](https://www.thingiverse.com/thing:28298), [PLOS ONE](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0059840))
  * [parametric cuvette/vial racks](https://www.thingiverse.com/thing:25080) [3D printing]
  * [3D printable adapter](https://www.thingiverse.com/thing:25080) to turn an automatic hammer turned in a [cell lysis](https://www.thermofisher.com/be/en/home/life-science/protein-biology/protein-biology-learning-center/protein-biology-resource-library/pierce-protein-methods/traditional-methods-cell-lysis.html) tool for DNA extraction [3D printing, automatic hammer]
  * [DremelFuge](https://www.thingiverse.com/thing:1483), low cost centrifuge <50$ [3D printing, Dremel]
  * [orbital shaker](https://www.thingiverse.com/thing:5045) [3D printing, Arduino]
  * [Parametric Automated Filter Wheel Changer](https://www.thingiverse.com/thing:26553) [3D printing, Arduino]  
  * [pHduino](https://phduino.blogspot.com/) (pH meter)
  * [Xoscillo](https://code.google.com/archive/p/xoscillo/) (Oscilloscope)
  * [OpenPCR](https://openpcr.org/) (DNA analysis)
  * [3D printed chemistry reactionware](https://www.nature.com/articles/nchem.1313)
  * [Open Raman spectroscopy](https://www.open-raman.org/)
  * [UC2 - Open and Modular Optical Toolbox](https://github.com/openUC2/UC2-GIT)

* Fabrication tools :
  * RepRap 3D printer [[paper](https://www.cambridge.org/core/journals/robotica/article/reprap-the-replicating-rapid-prototyper/5979FD7B0C066CBCE43EEAD869E871AA), [reprap.org](https://reprap.org/wiki/RepRap)]
  * [Lasersaur](https://www.lasersaur.com/)
  * [precious plastic](https://preciousplastic.com/)
  * [RecycleBot](https://www.appropedia.org/Recyclebot)

* companies selling open source hardware:
  * [OpenEphys](https://open-ephys.org/)
  * [Backyard Brains](https://backyardbrains.com/)
  * [Open PCR](https://www.chaibio.com/)

## Repositories/communities

* [GOSH](http://openhardware.science/about/)
* [Thingiverse](https://www.thingiverse.com/)
* [Instructables](https://www.instructables.com/)
* [Printables by Josef Prusa](https://www.printables.com/fr)
* [Fab Academy Archive](https://fabacademy.org/archive/)
* [Appropedia](https://www.appropedia.org/Welcome_to_Appropedia)
* [Low-Tech Lab](https://lowtechlab.org/)

## How to grow an open community ?

* The example of Open-Science:
  * [Worm and Human Genome](https://www.youtube.com/watch?v=CTwcYQ9WHOA)
  * [Open Science - the vision of the European Commission](https://research-and-innovation.ec.europa.eu/strategy/strategy-2020-2024/our-digital-future/open-science_en)

* Community
  * advantages : affordable, accessibility, reproducibility, ownership

* Action
  * Grow the community to reach critical mass
  * share your ideas
  * publish existing little projects/"machines"
  * publish "work in progress" (GitHub, preprints for faster dissemination,...)
  * learn and teach basic fabrication skills.

## Some open communities and movements

* [Fab Lab Network](https://fabfoundation.org/global-community/)
    * [[projects] GitLab repo](https://gitlab.fabcloud.org/pub/)
    * [[teaching] Academany](https://academany.org/)
      * [Fab Academy](https://fabacademy.org/)
* [Frugal Science](https://125.stanford.edu/frugal-science/)
   * [[projects] Prakash Lab](https://web.stanford.edu/group/prakash-lab/cgi-bin/labsite/research/frugal-science-and-global-health/)
   * [[teaching] Stanford - BioE271: Frugal Science](https://www.frugalscience.org/)
* [Open Science - the vision of the European Commission](https://research-and-innovation.ec.europa.eu/strategy/strategy-2020-2024/our-digital-future/open-science_en)
* [Low-tech](https://fr.wikipedia.org/wiki/Low-tech)
   * [[projects] Les tutos du Low-Tech Lab](https://lowtechlab.org/fr/la-low-tech)
   * [teaching](https://campus.we-explore.org/?CommentFaireMieuxAvecMoinsPenserAutreme)
* [Repair Cafés](https://repairtogether.be/)

## References

* [[book] J. Pearce, Open-source Lab](https://www.elsevier.com/books/open-source-lab/pearce/978-0-12-410462-4)
* [[paper] Open Source Toolkit: Hardware, PLOS blogs, 2015](https://collectionsblog.plos.org/open-source-toolkit-hardware/)
* [[paper] Open Labware: 3-D Printing Your Own Lab Equipment](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1002086)
* [[class] Frugal Science, BIOE271, Stanford University](https://www.frugalscience.org/)

