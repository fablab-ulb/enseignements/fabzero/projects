# Making the invisible visible

## Fab Academy

* [FabScope](https://github.com/satshas/FabScope)
* [Open Metrology](https://academy.cba.mit.edu/classes/applications_implications/open_metrology/index.html)
* [Puzzle pieces sorter](http://archive.fabacademy.org/archives/2017/woma/students/238/assignment16.html)

## DIY

* [Thermal imaging](https://petapixel.com/2021/03/25/how-to-build-your-own-thermal-camera-with-a-raspberry-pi/)
* [Cloud chamber](https://physicsopenlab.org/2017/05/05/cloud-chamber/)


## Open-source and frugal scientific tools:

* [PlanktoScope](https://www.planktoscope.org/),  a frugal high-throughput microscope platform
* Foldscope([website](https://foldscope.com/), [PLOS ONE](https://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0098781), [Ted Talk](https://www.ted.com/talks/manu_prakash_a_50_cent_microscope_that_folds_like_origami))
* [OpenFlexure Microscope](https://openflexure.org/)
* [Open Raman spectroscopy](https://www.open-raman.org/)
* [UC2 - Open and Modular Optical Toolbox](https://github.com/openUC2/UC2-GIT)
* [3D Paws](https://www.comet.ucar.edu/node/543), 3D printed automatic weather station

## FabZero-Experiments final projects

* [2022-2023](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/final-projects/final-projects/)
* [2021-2022](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/class-website/final-projects/final-projects/)
* [2020-2021](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/final-projects/final-projects/)

## Repositories/communities

* [GOSH](http://openhardware.science/about/)
* [Thingiverse](https://www.thingiverse.com/)
* [Instructables](https://www.instructables.com/)
* [Printables by Josef Prusa](https://www.printables.com/fr)
* [Fab Academy Archive](https://fabacademy.org/archive/)
* [Appropedia](https://www.appropedia.org/Welcome_to_Appropedia)
* [Low-Tech Lab](https://lowtechlab.org/)

## References

* [[book] J. Pearce, Open-source Lab](https://www.elsevier.com/books/open-source-lab/pearce/978-0-12-410462-4)
* [[paper] Open Source Toolkit: Hardware, PLOS blogs, 2015](https://collectionsblog.plos.org/open-source-toolkit-hardware/)
* [[paper] Open Labware: 3-D Printing Your Own Lab Equipment](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1002086)
* [[class] Frugal Science, BIOE271, Stanford University](https://www.frugalscience.org/)